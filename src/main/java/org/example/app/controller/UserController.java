package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserAuthRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.manager.UserManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.http.HttpMethods;
import org.example.framework.server.annotation.*;

@Component
@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserManager manager;

    @RequestMapping(method = HttpMethods.GET, path = "^/users/(?<id>\\d+)$")
    @ResponseBody
    public GetUserByIdRS getById(@PathVariable("id") final long id) {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/register$")
    @ResponseBody
    public UserRegisterRS register(@RequestBody final UserRegisterRQ requestDTO) {
        return manager.create(requestDTO);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/auth")
    @ResponseBody
    public UserAuthRS authenticate(@RequestBody final LoginPasswordAuthenticationToken requestDTO) {
        if (manager.authenticate(requestDTO)) {
            return new UserAuthRS("Successful authentication");
        }
        return new UserAuthRS("Failed authentication");
    }
}
