package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.manager.AvitoManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.http.HttpMethods;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.annotation.RequestBody;
import org.example.framework.server.annotation.RequestMapping;
import org.example.framework.server.annotation.ResponseBody;

import java.util.List;

@Component
@Controller
@RequiredArgsConstructor
public class AvitoController {
    private final AvitoManager manager;

    @RequestMapping(method = HttpMethods.GET,
            path = "^/avito/(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})$")
    @ResponseBody
    public AvitoGetByIdRS getById(@RequestBody final AvitoGetByIdRQ requestDTO) {
        return manager.getById(requestDTO);
    }

    @RequestMapping(method = HttpMethods.GET, path = "^/avito$")
    @ResponseBody
    public List<AvitoGetAllRS> getAll() {
        return manager.getAll();
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/avito$")
    @ResponseBody
    public AvitoCreateRS create(@RequestBody final AvitoCreateRQ requestDTO) {
        return manager.create(requestDTO);
    }

    @RequestMapping(method = HttpMethods.PUT,
            path = "^/avito/(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})$")
    @ResponseBody
    public AvitoUpdateRS update(@RequestBody final AvitoUpdateRQ requestDTO) {
        return manager.update(requestDTO);
    }

    @RequestMapping(method = HttpMethods.DELETE,
            path = "^/avito/(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})$")
    @ResponseBody
    public AvitoDeleteByIdRS delete(@RequestBody final AvitoDeleteByIdRQ requestDTO) {
        return manager.deleteById(requestDTO);
    }

}
