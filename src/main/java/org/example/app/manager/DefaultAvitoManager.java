package org.example.app.manager;

import org.example.app.domain.Apartment;
import org.example.app.dto.*;
import org.example.app.exception.NotFoundItemException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.security.proxy.Auth;
import org.example.framework.server.exception.MethodNotAllowedException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class DefaultAvitoManager implements AvitoManager {
    private final List<Apartment> items = new ArrayList<>(100);

    @Override
    @Audit
    public synchronized List<AvitoGetAllRS> getAll() {
        return items.stream()
                .map(o -> new AvitoGetAllRS(
                        o.getId(),
                        o.getOwner(),
                        o.getRoomsNumber(),
                        o.isStudio(),
                        o.isFreeLayout(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLoggia(),
                        o.getFloorNumber(),
                        o.getFloorNumberInHouse(),
                        o.getAddress(),
                        o.getDescription()
                ))
                .collect(Collectors.toList());
    }

    @Override
    public synchronized AvitoGetByIdRS getById(final AvitoGetByIdRQ requestDTO) {
        return items.stream()
                .filter(o -> o.getId().equals(requestDTO.getId()))
                .findAny()
                .map(o -> new AvitoGetByIdRS(o.getId(),
                        o.getOwner(),
                        o.getRoomsNumber(),
                        o.isStudio(),
                        o.isFreeLayout(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLoggia(),
                        o.getFloorNumber(),
                        o.getFloorNumberInHouse(),
                        o.getAddress(),
                        o.getDescription()))
                .orElseThrow(() -> new NotFoundItemException("item not found"));
    }

    @Override
    @HasRole("USER")
    @Audit
    public AvitoCreateRS create(final AvitoCreateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();

        if (!(principal instanceof LoginPrincipal)) {
            throw new MethodNotAllowedException("not a login principal");
        }

        final Apartment item = new Apartment(
                UUID.randomUUID().toString(),
                principal.getName(),
                requestDTO.getRoomsNumber(),
                requestDTO.isStudio(),
                requestDTO.isFreeLayout(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLoggia(),
                requestDTO.getFloorNumber(),
                requestDTO.getFloorNumberInHouse(),
                requestDTO.getAddress(),
                requestDTO.getDescription()
        );

        synchronized (this) {
            items.add(item);

            return new AvitoCreateRS(
                    item.getId(),
                    item.getRoomsNumber(),
                    item.isStudio(),
                    item.isFreeLayout(),
                    item.getPrice(),
                    item.getArea(),
                    item.isHasBalcony(),
                    item.isHasLoggia(),
                    item.getFloorNumber(),
                    item.getFloorNumberInHouse(),
                    item.getAddress(),
                    item.getDescription()
            );
        }
    }

    @Override
    @HasRole("USER")
    public AvitoUpdateRS update(final AvitoUpdateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<Apartment> item;
        synchronized (this) {
            item = items.stream()
                    .filter(o -> o.getId().equals(requestDTO.getId()))
                    .findAny();
        }
        item.orElseThrow(() -> new NotFoundItemException("item not found"));
        final Auth authRole = SecurityContext.getAuthRole();
        if (!authRole.getName().equals(requestDTO.getLogin())) {
            throw new MethodNotAllowedException("not an owner");
        }

        synchronized (this) {
            final int idx = getIdxById(requestDTO.getId());
            if (idx == -1) {
                throw new NotFoundItemException("item not found");
            }
            items.set(idx, new Apartment(requestDTO.getId(),
                    principal.getName(),
                    requestDTO.getRoomsNumber(),
                    requestDTO.isStudio(),
                    requestDTO.isFreeLayout(),
                    requestDTO.getPrice(),
                    requestDTO.getArea(),
                    requestDTO.isHasBalcony(),
                    requestDTO.isHasLoggia(),
                    requestDTO.getFloorNumber(),
                    requestDTO.getFloorNumberInHouse(),
                    requestDTO.getAddress(),
                    requestDTO.getDescription()));

            return new AvitoUpdateRS(
                    requestDTO.getId(),
                    requestDTO.getRoomsNumber(),
                    requestDTO.isStudio(),
                    requestDTO.isFreeLayout(),
                    requestDTO.getPrice(),
                    requestDTO.getArea(),
                    requestDTO.isHasBalcony(),
                    requestDTO.isHasLoggia(),
                    requestDTO.getFloorNumber(),
                    requestDTO.getFloorNumberInHouse(),
                    requestDTO.getAddress(),
                    requestDTO.getDescription()
            );
        }
    }

    @Override
    @HasRole("USER")
    public AvitoDeleteByIdRS deleteById(final AvitoDeleteByIdRQ requestDTO) {
        final Optional<Apartment> item;
        synchronized (this) {
            item = items.stream()
                    .filter(o -> o.getId().equals(requestDTO.getId()))
                    .findAny();
        }
        item.orElseThrow(() -> new NotFoundItemException("item not found"));
        final Apartment apartment = item.get();
        final Auth authRole = SecurityContext.getAuthRole();
        if (!authRole.getName().equals(requestDTO.getLogin())) {
            throw new MethodNotAllowedException("not an owner");
        }

        synchronized (this) {
            int idx = getIdxById(requestDTO.getId());
            if (idx == -1) {
                throw new NotFoundItemException("item not found");
            }
            items.remove(idx);
            return new AvitoDeleteByIdRS(
                    apartment.getId(),
                    apartment.getRoomsNumber(),
                    apartment.isStudio(),
                    apartment.isFreeLayout(),
                    apartment.getPrice(),
                    apartment.getArea(),
                    apartment.isHasBalcony(),
                    apartment.isHasLoggia(),
                    apartment.getFloorNumber(),
                    apartment.getFloorNumberInHouse(),
                    apartment.getAddress(),
                    apartment.getDescription()
            );
        }
    }


    private synchronized int getIdxById(String id) {
        int idx = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                idx = i;
            }
        }
        return idx;
    }
}
