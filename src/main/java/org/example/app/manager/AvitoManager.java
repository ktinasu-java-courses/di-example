package org.example.app.manager;

import org.example.app.dto.*;

import java.util.List;

public interface AvitoManager {
    List<AvitoGetAllRS> getAll();

    AvitoGetByIdRS getById(AvitoGetByIdRQ requestDTO);

    AvitoCreateRS create(AvitoCreateRQ requestDTO);

    AvitoUpdateRS update(AvitoUpdateRQ requestDTO);

    AvitoDeleteByIdRS deleteById(AvitoDeleteByIdRQ requestDTO);
}
