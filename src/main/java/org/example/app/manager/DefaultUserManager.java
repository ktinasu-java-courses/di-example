package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.User;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.CantReadFilesException;
import org.example.app.exception.NotFoundItemException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.exception.BadRegisterDataException;
import org.example.framework.server.exception.LoginAlreadyRegisteredException;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DefaultUserManager implements UserManager {
    private final AtomicLong nextId = new AtomicLong(1);
    private static final Map<String, User> usersByLogin = new HashMap<>();
    private static final Map<Long, User> usersById = new HashMap<>();
    private final PasswordEncoder encoder = new Argon2PasswordEncoder();

    private List<String> badLogins;
    private List<String> badPasswords;

    public DefaultUserManager() {
        readFiles();
    }

    @Override
    public GetUserByIdRS getById(final long id) {
        final Optional<User> user;
        synchronized (this) {
            user = Optional.ofNullable(usersById.get(id));
        }

        return user
                .map(o -> new GetUserByIdRS(o.getId(), o.getLogin()))
                .orElseThrow(NotFoundItemException::new)
                ;
    }

    @Override
    @Audit
    public UserRegisterRS create(final UserRegisterRQ requestDTO) {
        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String password = requestDTO.getPassword().trim().toLowerCase();

        try {
            final boolean matchLogin = badLogins.stream().anyMatch(o -> o.equals(login));
            if (matchLogin) {
                throw new BadRegisterDataException("Bad login");
            }
            final boolean matchPassword = badPasswords.stream().anyMatch(o -> o.equals(password));
            if (matchPassword) {
                throw new BadRegisterDataException("Password is in 10 000 leaked passwords");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new CantReadFilesException("Can't find file");
        }

        final String encodedPassword = encoder.encode(requestDTO.getPassword());
        final User user = User.builder()
                .id(nextId.getAndIncrement())
                .login(login)
                .passwordHash(encodedPassword)
                .build();

        synchronized (this) {
            if (usersByLogin.containsKey(login)) {
                log.error("registration with same login twice: {}", login);
                throw new LoginAlreadyRegisteredException();
            }

            usersByLogin.put(user.getLogin(), user);
            usersById.put(user.getId(), user);
        }

        return new UserRegisterRS(user.getId(), user.getLogin());
    }

    @Override
    @Audit
    public boolean authenticate(final AuthenticationToken request) {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }

        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();

        final String encodedPassword;
        synchronized (this) {
            if (!usersByLogin.containsKey(login)) {
                return false;
            }

            encodedPassword = usersByLogin.get(login).getPasswordHash();
        }
        return encoder.matches(password, encodedPassword);
    }

    private void readFiles() {
        try {
            badLogins = Files.newBufferedReader(Paths.get("bad-logins.txt"))
                    .lines()
                    .collect(Collectors.toList());
            badPasswords = Files.newBufferedReader(Paths.get("leaked-passwords.txt"))
                    .lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            throw new CantReadFilesException("Can't read files");
        }
    }
}
